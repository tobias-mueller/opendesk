{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apps:
  clamavDistributed:
    enabled: {{ .Values.clamavDistributed.enabled }}
  clamavSimple:
    enabled: {{ .Values.clamavSimple.enabled }}
  collabora:
    enabled: {{ .Values.collabora.enabled }}
  cryptpad:
    enabled: {{ .Values.cryptpad.enabled }}
  dovecot:
    enabled: {{ .Values.dovecot.enabled }}
  element:
    enabled: {{ .Values.element.enabled }}
  intercom:
    enabled: {{ .Values.intercom.enabled }}
  jitsi:
    enabled: {{ .Values.jitsi.enabled }}
  mariadb:
    enabled: {{ .Values.mariadb.enabled }}
  memcached:
    enabled: {{ .Values.memcached.enabled }}
  minio:
    enabled: {{ .Values.minio.enabled }}
  nextcloud:
    enabled: {{ .Values.nextcloud.enabled }}
  openproject:
    enabled: {{ .Values.openproject.enabled }}
  oxAppsuite:
    enabled: {{ .Values.oxAppsuite.enabled }}
  oxConnector:
    enabled: {{ .Values.oxConnector.enabled }}
  postfix:
    enabled: {{ .Values.postfix.enabled }}
  postgresql:
    enabled: {{ .Values.postgresql.enabled }}
  redis:
    enabled: {{ .Values.redis.enabled }}
  univentionManagementStack:
    enabled: {{ .Values.univentionManagementStack.enabled }}
  xwiki:
    enabled: {{ .Values.xwiki.enabled }}

ingressController:
  {{ .Values.security.ingressController | toYaml | nindent 2 }}


extraApps:
  clusterPostfix:
    enabled: {{ .Values.security.clusterPostfix.enabled }}
    namespace: {{ .Values.security.clusterPostfix.namespace }}
...
