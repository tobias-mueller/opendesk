# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
apiVersion: "kyverno.io/v1"
kind: "ClusterPolicy"
metadata:
  name: "disallow-host-ports"
  annotations:
    policies.kyverno.io/title: "Disallow hostPorts"
    policies.kyverno.io/subject: "Pod"
    policies.kyverno.io/description: >-
      Access to host ports allows potential snooping of network traffic and should not be allowed, or at minimum
      restricted to a known list. This policy ensures the `hostPort` field is unset or set to `0`.
spec:
  background: true
  rules:
    - name: "disallow-host-ports"
      match:
        any:
          - resources:
              kinds:
                - "Pod"
      validate:
        message: >-
          Use of host ports is disallowed. The fields spec.containers[*].ports[*].hostPort
          , spec.initContainers[*].ports[*].hostPort, and spec.ephemeralContainers[*].ports[*].hostPort
          must either be unset or set to `0`.
        pattern:
          spec:
            =(ephemeralContainers):
              - =(ports):
                  - =(hostPort): 0
            =(initContainers):
              - =(ports):
                  - =(hostPort): 0
            containers:
              - =(ports):
                  - =(hostPort): 0
